<?php

/**
 * @file
 * Commerce Xero Post Updates.
 */

use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\Core\Config\Entity\ConfigEntityUpdater;

/**
 * Updates Commerce Xero Strategy entities. Some will be disabled.
 */
function commerce_xero_post_update_account_entities(&$sandbox) {

  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater
    ->update($sandbox, 'commerce_xero_strategy', function (CommerceXeroStrategyInterface $strategy) {
      $bankAccount = $strategy->get('bank_account');
      $revenueAccount = $strategy->get('revenue_account');
      $needs_save = is_string($bankAccount) || is_string($revenueAccount);

      if (is_string($bankAccount)) {
        // The bank account needs to be emptied out if empty string, but that's
        // bad data anyway.
        $strategy->set('bank_account', ['AccountID' => '', 'Name' => 'ERROR']);
        $strategy->set('status', FALSE);
      }

      if (is_string($revenueAccount)) {
        $account = [
          'Code' => $revenueAccount,
          'Name' => $revenueAccount,
        ];
        $strategy->set('revenue_account', $account);
      }

      return $needs_save;
    });

  return t('You must reconfigure and re-export all strategy entities.');
}
