# Development notes for Drupal 8

* [META: Development Plan for Commerce Xero](https://www.drupal.org/node/1949288)

## Strategy pattern

There are different strategies that commerce site owners would want to use to
integrate their commerce orders and payment transactions with their Xero bank
statements or invoices.

Commerce Xero should provide default strategies and a way to create new
strategies.

A strategy is related to

* a Xero data type e.g. Invoice, Bank Transaction, Invoice-Payment
* an unique Commerce payment gateway e.g. Auth.net, Stripe, Manual payment,
  PayPal, etc...
* a Xero revenue account
* a Xero bank account
* or it is possible that a developer would want to come up with multiple
  strategies using one payment method and different revenue/bank accounts and
  create code to choose the strategy?

A strategy contains a configurable, ordered set of plugins to execute by
Commerce event to

* Reconcile tax calculations that vary between Commerce and Xero :*(
  * Line amount type makes this difficult, and most likely the user should make
    Xero the system of record.
* Reconcile Contact on the order
* Reconcile Tracking Categories for each order item
* Store Xero reference object on the order item so that it can be updated and
  referenced
   * Order item or order type? Or Payment?

## Strategy resolver

Commerce Xero should provide a strategy resolver, which has a fallback based on
the current resolver in Drupal 7 (payment method) and allows to chain other
custom resolvers first.

A Strategy resolver needs to

* return the Strategy entity to use to process the order.

## Processor plugin

A Processor plugin refers to plugins that are run on Commerce event and added to
a strategy entity.

A processor has

* a process method that does what it needs to
   * Perhaps borrow from Serializer component and use a $context parameter to pass in contextual data like the Typed Data data to process?
* a base plugin class that injects xero.query.factory service.
* a plugin manager that instantiates plugins for a strategy.

```graphviz
digraph G {
  labelloc=t
  rankdir=LR

  "Commerce Event" -> Resolver -> Strategy -> "Event Plugins" -> Plugin -> Process;
}
```

### Queue

Similar pattern to 7.x-1.x, but could look at batching for API limits, and
whenever a queue item is to be inserted, look at adding it to an unclaimed item.

## TODO

* [x] Reimplement processor invoke via action.
* [x] Reimplement queue process plugin for a strategy based on various data.
* [ ] Implement a chain resolver to replace the basic resolver.
* [x] Reimplement bank transaction suppport via processor plugin.
* [x] Reimplement invoice support via processor plugin.
   * [ ] Split into Invoice and Payment processor plugins.
* [ ] Implement Order processors.
* [x] Implement processor plugins for TrackingCategories.
* [ ] Implement processor plugin for Tax.
   * Currently the tax processing is okay for Xero Accounting if the revenue
     account code is suitable for the taxation needed. However a processor
     plugin could modify the account code to map to various tax adjustments in
     Commerce.
* [ ] Implement processor plugin for Contact (search/update existing contact).
* [ ] Implement batching as a queue process plugin.

## Development Environment

There are plenty of development environment setups based on your preferences
from virtual machines to native solutions.

### Getting code

Clone core and module dependencies in case you need to work on Drupal core or
any of this module's dependencies.

    composer create-project drupal/recommended-project:^10 commerce-xero
    cd commerce-xero
    composer config minimum-stability alpha
    composer config extra.preferred-install."drupal/xero" source
    composer require drupal/xero drupal/commerce drush/drush
    git clone --branch 2.0.x https://git.drupal.org/project/commerce_xero.git modules/commerce_xero

### Xero API

1. Create a [Xero account](https://developer.xero.com/documentation/getting-started/development-accounts) using a Demo company.
2. Create a New app on the [Developer Portal](https://developer.xero.com/app/manage/).
3. Configure [Xero API module](https://drupal.org/project/xero) on your development site.

### Commerce

Configure a store with checkout, cart, order, product, tax, payment example modules.

## Tests

Try to create Unit tests as either Unit or Kernel tests rather than Functional,
Functional-Javascript or Nightwatch tests.

Each new plugin, entity type, strategy resolver, etc... should have an unit
test. Unit tests for UI elements isn't strictly necessary for a patch.
