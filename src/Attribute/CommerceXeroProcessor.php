<?php

namespace Drupal\commerce_xero\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Commerce Xero processor plugin attribute.
 *
 * Plugin namespace: Plugin\CommerceXero\processor.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class CommerceXeroProcessor extends Plugin {

  /**
   * Initialization method.
   *
   * @param string $id
   *   The plugin identifier.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The plugin label.
   * @param string[] $types
   *   The data types that this plugin handles.
   * @param string $execution
   *   The execution state that this plugin will fire from. This can be one of
   *   the following values:
   *     - immediate: process the data type as it is created.
   *     - process: process the data type in the "process" queue.
   *     - send: process the data type in the "send" queue before it is sent.
   * @param bool $required
   *   (optional) Makes a plugin required on any strategy.
   * @param mixed[] $settings
   *   (optional) Plugin configuration.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly array $types,
    public readonly string $execution,
    public readonly bool $required = FALSE,
    public readonly array $settings = [],
  ) {}

}
