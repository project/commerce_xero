<?php

namespace Drupal\commerce_xero\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Commerce Xero data_type plugin attribute.
 *
 * Plugin namespace: Plugin\CommerceXero\type.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class CommerceXeroDataType extends Plugin {

  /**
   * Initialization method.
   *
   * @param string $id
   *   The plugin identifier.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The plugin label.
   * @param string $type
   *   The Typed Data data type plugin identifier.
   * @param array $settings
   *   (optional) An array of settings.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly string $type,
    public readonly array $settings = [],
  ) {}

}
