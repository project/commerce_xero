<?php

// @phpcs:disable Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps

namespace Drupal\commerce_xero\Entity;

use Drupal\commerce_xero\CommerceXeroProcessorPluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Commerce Xero strategy entity interface.
 */
interface CommerceXeroStrategyInterface extends ConfigEntityInterface {

  /**
   * Gets the Bank Account ID from the bank account settings.
   *
   * @return string
   *   The Bank Account GUID.
   */
  public function getBankAccountID(): string;

  /**
   * Gets the Revenue Account Code from the revenue account settings.
   *
   * @return string
   *   The Revenue Account Code.
   */
  public function getRevenueAccountCode(): string;

  /**
   * Gets the enabled plugins and settings.
   *
   * @return array
   *   An associative array keyed by the plugin id.
   */
  public function getEnabledPlugins(): array;

  /**
   * Gets the plugin settings for the specified plugin ID.
   *
   * @param string $plugin_id
   *   The processor plugin ID.
   *
   * @return array|null
   *   Returns the plugin mapping object/array or NULL.
   */
  public function getEnabledPlugin(string $plugin_id): ?array;

  /**
   * Get the plugin weight for the plugin.
   *
   * @param \Drupal\commerce_xero\CommerceXeroProcessorPluginInterface $plugin
   *   The plugin instance.
   *
   * @return int
   *   The plugin weight.
   */
  public function getPluginWeight(CommerceXeroProcessorPluginInterface $plugin): int;

}
