<?php

namespace Drupal\commerce_xero;

use Drupal\Core\TypedData\ComplexDataInterface;

/**
 * A compact model for tracking strategy, data object and payment.
 */
class CommerceXeroData implements CommerceXeroDataInterface {

  /**
   * Strategy entity ID.
   *
   * @var string
   */
  protected string $strategyId;

  /**
   * Commerce payment entity ID.
   *
   * @var int|string
   */
  protected int|string $paymentId;

  /**
   * Typed data object.
   *
   * @var \Drupal\Core\TypedData\ComplexDataInterface|null
   */
  protected ?ComplexDataInterface $data;

  /**
   * Execution state.
   *
   * @var string
   */
  protected string $state;

  /**
   * The poison count.
   *
   * @var int
   */
  public $count;

  /**
   * Initialize method.
   *
   * @param string $strategyId
   *   The strategy ID.
   * @param int|string $paymentId
   *   The payment ID.
   * @param \Drupal\Core\TypedData\ComplexDataInterface|null $data
   *   The typed data object.
   * @param string $state
   *   The execution state: "process" or "send", but not "immediate".
   */
  public function __construct(string $strategyId, int|string $paymentId, ?ComplexDataInterface $data = NULL, string $state = 'process') {
    $this->strategyId = $strategyId;
    $this->paymentId = $paymentId;
    $this->data = $data;
    $this->state = $state;
    $this->count = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getStrategyEntityId(): string {
    return $this->strategyId;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentEntityId(): int {
    return $this->paymentId;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): ComplexDataInterface {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function setData(ComplexDataInterface $data): void {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getExecutionState(): string {
    return $this->state;
  }

  /**
   * {@inheritdoc}
   */
  public function setExecutionState($state = 'process'): void {
    if (!in_array($state, ['process', 'send', 'poison'])) {
      throw new \InvalidArgumentException('Illegal value for execution state');
    }
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function incrementCount(): void {
    $this->count = $this->count + 1;
  }

  /**
   * {@inheritdoc}
   */
  public function exceededPoisonThreshhold(): bool {
    return $this->count > self::POISON_THRESHHOLD;
  }

}
