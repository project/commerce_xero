<?php

namespace Drupal\commerce_xero;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;

/**
 * Describes the methods necessary to resolve strategies.
 */
interface CommerceXeroStrategyResolverInterface {

  /**
   * Finds appropriate commerce xero strategy for a payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The Commerce payment entity.
   *
   * @return \Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface|null
   *   A strategy interface to use.
   */
  public function resolve(PaymentInterface $payment): ?CommerceXeroStrategyInterface;

}
