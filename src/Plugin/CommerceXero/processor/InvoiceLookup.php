<?php

namespace Drupal\commerce_xero\Plugin\CommerceXero\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Attribute\CommerceXeroProcessor;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\xero\XeroQueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides processor plugin to lookup invoice by the order number or reference.
 *
 * @CommerceXeroProcessor(
 *   id = "commerce_xero_invoice_lookup",
 *   label = @Translation("Invoice lookup"),
 *   types = { "xero_invoice" },
 *   settings = { },
 *   execution = "immediate",
 *   required = FALSE
 * )
 */
#[CommerceXeroProcessor(
  id: 'commerce_xero_invoice_lookup',
  label: new TranslatableMarkup('Invoice lookup'),
  types: ['xero_invoice'],
  execution: 'immediate',
)]
class InvoiceLookup extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  use TypedDataTrait;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Xero query builder.
   *
   * @var \Drupal\xero\XeroQuery
   */
  protected $query;

  /**
   * Initialization method.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed_data_manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger.factory service.
   * @param \Drupal\xero\XeroQueryFactory $xeroQueryFactory
   *   The xero.query.factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TypedDataManagerInterface $typedDataManager, LoggerChannelFactoryInterface $loggerFactory, XeroQueryFactory $xeroQueryFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setTypedDataManager($typedDataManager);
    $this->logger = $loggerFactory->get('commerce_xero');
    $this->query = $xeroQueryFactory->get();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('typed_data_manager'),
      $container->get('logger.factory'),
      $container->get('xero.query.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy): bool {
    if ($data instanceof XeroComplexItemInterface) {
      $order = $payment->getOrder();
      $this
        ->query
        ->setType('xero_invoice')
        ->setMethod('get')
        ->addCondition('Reference', $order->id(), '==');

      if ($order->getOrderNumber()) {
        $this
          ->query
          ->addOperator('OR')
          ->addCondition('InvoiceNumber', $order->getOrderNumber(), '==');
      }

      $statuses = ['DRAFT', 'SUBMITTED', 'AUTHORISED'];
      /** @var \Drupal\xero\Plugin\DataType\XeroItemList $invoices */
      $invoices = $this->query->execute();
      $found = NULL;
      if ($invoices && !$invoices->isEmpty()) {
        foreach ($invoices as $invoice) {
          if (in_array($invoice->get('Status')->getValue(), $statuses)) {
            $found = $invoice;
            break;
          }
        }
      }

      if ($found) {
        $data = $found;
        $this->logger->notice('Invoice data updated based on InvoiceId {id}', [
          'id' => $found->get('InvoiceID')->getValue(),
        ]);

      }
      else {
        $this->logger->notice('Invoice not found for order {id} or order number {number}', [
          'id' => $order->id(),
          'number' => $order->getOrderNumber() ?? 'not defined',
        ]);
      }
    }

    return TRUE;
  }

}
