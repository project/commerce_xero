<?php

namespace Drupal\commerce_xero\Plugin\CommerceXero\processor;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Attribute\CommerceXeroProcessor;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\xero\XeroItemManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Posts the data to Xero.
 *
 * @CommerceXeroProcessor(
 *   id = "commerce_xero_send",
 *   label = @Translation("Posts to Xero"),
 *   types = {},
 *   execution = "send",
 *   settings = {},
 *   required = TRUE
 * )
 */
#[CommerceXeroProcessor(
  id: 'commerce_xero_send',
  label: new TranslatableMarkup('Posts to Xero'),
  types: [],
  execution: 'send',
  required: TRUE,
  settings: [],
)]
class PostToXero extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  use TypedDataTrait;

  /**
   * The Xero API Item Manager.
   *
   * @var \Drupal\xero\XeroItemManagerInterface
   */
  protected $itemManager;

  /**
   * Initialize method.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\xero\XeroItemManagerInterface $itemManager
   *   The xero.item_manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, XeroItemManagerInterface $itemManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->itemManager = $itemManager;
    $this->typedDataManager = $this->getTypedDataManager();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy): bool {
    if ($data instanceof XeroComplexItemInterface) {
      $created = NULL;
      $has_guid = $data->get($data::getXeroProperty('guid_name'))->getValue();
      if ($has_guid) {
        $created = $this->itemManager->updateItem($data);
      }
      else {
        $created = $this->itemManager->createItem($data);
      }

      if ($created) {
        // Sets the xero reference on the payment. This should always return
        // true unless overridden.
        $hasField = $payment->getFieldDefinition('xero_transaction');
        if ($hasField !== NULL) {
          $guid = $created->get($created->getXeroProperty('guid_name'))->getValue();

          $values = [
            'guid' => $guid,
            'label' => $guid,
            'type' => $created->getDataDefinition()->getDataType(),
          ];

          if ($payment->get('xero_transaction')->count() > 0) {
            $payment->get('xero_transaction')->appendItem($values);
          }
          else {
            $payment->set('xero_transaction', $values, TRUE);
          }

          try {
            $payment->save();
          }
          catch (EntityStorageException $e) {
            return FALSE;
          }
        }
      }
      else {
        return FALSE;
      }

      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('xero.item_manager')
    );
  }

}
