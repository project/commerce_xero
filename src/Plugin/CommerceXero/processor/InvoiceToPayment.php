<?php

namespace Drupal\commerce_xero\Plugin\CommerceXero\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Attribute\CommerceXeroProcessor;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\xero\XeroItemManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides invoice to payment processor plugin to complete xero invoice.
 *
 * @CommerceXeroProcessor(
 *   id = "commerce_xero_invoice_to_payment",
 *   label = @Translation("Invoice to payment"),
 *   types = { "xero_invoice" },
 *   execution = "process",
 *   settings = { },
 *   required = FALSE,
 * )
 */
#[CommerceXeroProcessor(
  id: 'commerce_xero_invoice_to_payment',
  label: new TranslatableMarkup('Invoice to payment'),
  types: ['xero_invoice'],
  execution: 'process',
)]
class InvoiceToPayment extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  use TypedDataTrait;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The xero item manager.
   *
   * @var \Drupal\xero\XeroItemManagerInterface
   */
  protected $itemManager;

  /**
   * Initialization method.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed_data_manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger.factory service.
   * @param \Drupal\xero\XeroItemManagerInterface $xeroItemManager
   *   The xero.item_manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TypedDataManagerInterface $typedDataManager, LoggerChannelFactoryInterface $loggerFactory, XeroItemManagerInterface $xeroItemManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setTypedDataManager($typedDataManager);
    $this->logger = $loggerFactory->get('commerce_xero');
    $this->itemManager = $xeroItemManager;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['notice'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['messages', 'warning'],
      ],
      'markup' => [
        '#markup' => $this->t('This plugin mutates the invoice into a payment and should be ordered after any plugins that process the invoice.'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy): bool {
    if ($data instanceof XeroComplexItemInterface) {
      $invoice = $data;
      if (!$data->get('InvoiceID')->getValue()) {
        $invoice = $this->itemManager->createItem($data);
        if (!$invoice) {
          $this->logger->warning('Failed to create invoice data type during invoice_to_payment');
          return FALSE;
        }
      }

      if ($invoice->get('InvoiceID')->getValue()) {
        $payment_definition = $this->typedDataManager
          ->createDataDefinition('xero_payment');
        $date = $payment->isCompleted() ? $payment->getCompletedTime() : time();
        $values = [
          'Invoice' => ['InvoiceID' => $invoice->get('InvoiceID')->getValue()],
          'Account' => ['AccountID' => $strategy->getBankAccountID()],
          'Date' => date('Y-m-d', $date),
          'Amount' => $payment->getAmount()->getNumber(),
          'Reference' => $payment->getRemoteId() ? $payment->getRemoteId() : $payment->getPaymentGateway()->label(),
          'Status' => 'AUTHORISED',
        ];

        // And... Mutate!
        $data = $this->typedDataManager->create($payment_definition, $values);

        $order = $payment->getOrder();
        if (!$order->getOrderNumber()) {
          // Also save the auto-generated invoice number if we don't have one
          // already.
          $order->setOrderNumber($invoice->get('InvoiceNumber')->getValue());
        }

        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('typed_data_manager'),
      $container->get('logger.factory'),
      $container->get('xero.item_manager')
    );
  }

}
