<?php

namespace Drupal\commerce_xero\Plugin\CommerceXero\type;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Attribute\CommerceXeroDataType;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\OrderDataTransformationTrait;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroDataTypePluginBase;
use Drupal\xero\TypedData\XeroItemInterface;

/**
 * Bank Transaction data type processor plugin.
 *
 * @CommerceXeroDataType(
 *   id = "commerce_xero_bank_transaction",
 *   label = @Translation("Bank Transaction"),
 *   type = "xero_bank_transaction",
 *   settings = { }
 * )
 */
#[CommerceXeroDataType(
  id: 'commerce_xero_bank_transaction',
  label: new TranslatableMarkup('Bank Transaction'),
  type: 'xero_bank_transaction',
)]
class BankTransaction extends CommerceXeroDataTypePluginBase {

  use OrderDataTransformationTrait;

  /**
   * {@inheritdoc}
   */
  public function make(PaymentInterface $payment, CommerceXeroStrategyInterface $strategy): XeroItemInterface {
    $configuration = $this->getConfiguration();
    $typedDataManager = $this->getTypedDataManager();
    $definition = $this->typedDataManager
      ->createDataDefinition($configuration['type']);

    // Sets the payment date depending on the payment state.
    $date = $payment->isCompleted() ? $payment->getCompletedTime() : \time();
    $order = $payment->getOrder();

    $data = [
      'Type' => 'RECEIVE',
      'BankAccount' => ['AccountID' => $strategy->getBankAccountID()],
      'Contact' => $this->getDefaultContactValues($order),
      'Date' => date('Y-m-d', $date),
      'Total' => $payment->getAmount()->getNumber(),
      'Reference' => $order->getOrderNumber(),
      'Url' => Url::fromUri(
        'entity:commerce_order/' . $order->id(),
        ['absolute' => TRUE])->toString(),
      'LineAmountTypes' => 'Inclusive',
      'LineItems' => [
        [
          'Description' => 'Order ' . $order->getOrderNumber() . ' Payment ' . $payment->id(),
          'UnitAmount' => $payment->getAmount()->getNumber(),
          'AccountCode' => $strategy->getRevenueAccountCode(),
        ],
      ],
    ];

    $item = $typedDataManager->create($definition, $data);
    assert($item instanceof XeroItemInterface);
    return $item;
  }

}
