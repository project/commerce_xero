<?php

namespace Drupal\commerce_xero;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the Commerce Xero Strategy entity.
 */
class StrategyAccessControlHandler extends EntityAccessControlHandler {}
