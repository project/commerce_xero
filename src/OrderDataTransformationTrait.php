<?php

namespace Drupal\commerce_xero;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides helper methods for data type plulgins.
 */
trait OrderDataTransformationTrait {

  /**
   * Gets default contact values from the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return array
   *   An array to set on the Contact property.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getDefaultContactValues(OrderInterface $order) {
    $profile = $order->getBillingProfile();
    if ($profile && $profile->hasField('address')) {
      // Gets the Contact information from the Address fields.
      /** @var \Drupal\address\AddressInterface $address */
      $address = $profile->address->first();
      $name = $address->getOrganization()
        ? $address->getOrganization()
        : $address->getGivenName() . ' ' . $address->getFamilyName();

      return [
        'FirstName' => $address->getGivenName(),
        'LastName' => $address->getFamilyName(),
        'Name' => $name,
        'EmailAddress' => $order->getEmail(),
      ];
    }

    // Otherwise get basic contact information from the order.
    return [
      'Name' => $order->getCustomer()->getAccountName(),
      'EmailAddress' => $order->getEmail(),
    ];
  }

}
