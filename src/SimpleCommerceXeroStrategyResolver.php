<?php

namespace Drupal\commerce_xero;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Basic commerce xero strategy resolution.
 */
class SimpleCommerceXeroStrategyResolver implements CommerceXeroStrategyResolverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Initialize method.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager sevice.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(PaymentInterface $payment): ?CommerceXeroStrategyInterface {
    /** @var \Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface[] $strategies */
    $strategies = $this->entityTypeManager
      ->getStorage('commerce_xero_strategy')
      ->loadByProperties([
        'payment_gateway' => $payment->getPaymentGatewayId(),
      ]);
    return $strategies ? reset($strategies) : NULL;
  }

}
