<?php

namespace Drupal\Tests\commerce_xero\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional test for adding Xero strategy.
 *
 * @group commerce_xero
 */
class AddXeroStrategyTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_store',
    'commerce_order',
    'commerce_payment',
    'commerce_payment_example',
    'serialization',
    'commerce_xero',
    'xero',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Adds xero data to cache to avoid querying Xero or setting up a fake Xero
    // app for functional tests.
    /** @var \Drupal\Core\TypedData\TypedDataManagerInterface $manager */
    $manager = $this->container->get('typed_data_manager');
    $definition = $manager->createListDataDefinition('xero_account');

    $accounts = $manager->create($definition, []);
    $bank_accounts = $manager->create($definition, []);

    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = $this->container->get('cache.xero_query');
    $cache->set('commerce_xero_bank_accounts', $bank_accounts);
    $cache->set('commerce_xero_accounts', $accounts);

    $privileged_user = $this->createUser([
      'administer commerce xero',
    ]);

    $this->drupalLogin($privileged_user);
  }

  /**
   * Tests adding a Xero strategy.
   */
  public function testProcessorsAvailable(): void {
    $this->drupalGet('/admin/commerce/config/xero/strategy/add');

    $this
      ->assertSession()
      ->checkboxChecked('Posts to Xero');

    $this
      ->assertSession()
      ->checkboxNotChecked('Adds Tracking Category');

    $this
      ->assertSession()
      ->checkboxNotChecked('Invoice to payment');

    $this
      ->assertSession()
      ->checkboxNotChecked('Invoice lookup');
  }

}
