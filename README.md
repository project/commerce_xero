# Commerce Xero

Provides integration between your Drupal Commerce site and Xero accounting system.

**Warning** Iterative development is on-going, and this module may or may not be production-ready for varying uses of the term "production-ready". See [DEVELOPER.md](./DEVELOPER.md) for details.

The module should work well for a simple store for a region where Xero is configured for the same tax as on the store.

## Requirements

* Install code via composer:

`composer require drupal/commerce_xero`

* Install code via git for development:

`composer create-project drupal/recommended-project:^10 commerce-xero`
`cd commerce-xero`
`composer config minimum-stability alpha`
`composer require drupal/xero drupal/commerce drush/drush`

## Permissions

* Cron requires that the "access xero" permission be granted to the anonymous user.
